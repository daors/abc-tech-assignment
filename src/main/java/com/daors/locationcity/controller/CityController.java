package com.daors.locationcity.controller;

import com.daors.locationcity.entity.City;
import com.daors.locationcity.service.CityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/city")
public class CityController {

    private CityServiceImpl cityService;

    @Autowired
    public CityController(CityServiceImpl theCityService){
        cityService = theCityService;
    }

    @GetMapping("/cities")
    public String listCities(Model theModel){

        List<City> theCities = cityService.findAll();
        theModel.addAttribute("cities", theCities);

        return "list-cities";
    }

//    @GetMapping("/cities")
//    public List<City> findAllCities(){
//        List<City> cities = cityService.findAll();
//
//        return cities;
//    }
}
