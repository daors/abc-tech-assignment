package com.daors.locationcity.service;

import com.daors.locationcity.entity.City;
import com.daors.locationcity.entity.Location;

import java.util.List;

public interface CityService {

    List<City> findAll();

    City findById(int theId);

    void save(City theCity);

    void deleteById(int theId);
}
