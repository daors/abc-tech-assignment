package com.daors.locationcity.entity;

import javax.persistence.*;

@Entity
@Table(name = "city")
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "city_id")
    private int cityId;

    private String name;

    public City(){}

    public City(String name) {
        this.name = name;
    }

    public int getId() {
        return cityId;
    }

    public void setId(int id) {
        this.cityId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + cityId +
                ", name='" + name + '\'' +
                '}';
    }
}
