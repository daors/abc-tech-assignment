package com.daors.locationcity.dao;

import com.daors.locationcity.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface LocationRepository extends JpaRepository<Location, Integer> {

    List<Location> findAllByOrderByNameAsc();
}
