package com.daors.locationcity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LocationcityApplication {

    public static void main(String[] args) {
        SpringApplication.run(LocationcityApplication.class, args);
    }

}
