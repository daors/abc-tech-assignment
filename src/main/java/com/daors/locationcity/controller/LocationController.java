package com.daors.locationcity.controller;

import com.daors.locationcity.entity.City;
import com.daors.locationcity.entity.Location;
import com.daors.locationcity.service.CityServiceImpl;
import com.daors.locationcity.service.LocationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/location")
public class LocationController {

    private LocationServiceImpl locationService;
    private CityServiceImpl cityService;

    @Autowired
    public LocationController(LocationServiceImpl theLocationServiceImpl, CityServiceImpl theCityServiceImpl){
        locationService = theLocationServiceImpl;
        cityService = theCityServiceImpl;
    }


    @GetMapping("/showFormForAdd")
    public String showFormForAdd(Model theModel){

        Location theLocation = new Location();
        List<City> theCities = cityService.findAll();

        theModel.addAttribute("location", theLocation);
        theModel.addAttribute("cities", theCities);

        return "locations/location-form";
    }

    @GetMapping("/showFormForUpdate")
    public String showFormForUpdate(@RequestParam("locationId") int theId, Model theModel){
        Location theLocation = locationService.findById(theId);
        List<City> theCities = cityService.findAll();

        theModel.addAttribute("location", theLocation);
        theModel.addAttribute("cities", theCities);

        return "locations/location-form";
    }


    @PostMapping("/save")
    public String saveEmployee(@ModelAttribute("location") Location theLocation){
        locationService.save(theLocation);

        return "redirect:/location/getAllLocations";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("locationId") int theId){
        locationService.deleteById(theId);

        return "redirect:/location/getAllLocations";
    }

    @GetMapping("/details")
    public String details(@RequestParam("locationId") int theId, Model theModel){
        Location theLocation = locationService.findById(theId);

        theModel.addAttribute("location", theLocation);

        return "locations/location-details";
    }

    @GetMapping("/getAllLocations")
    public String getAllLocations(Model theModel){

        List<Location> theLocations = locationService.findAll();

        theModel.addAttribute("locations", theLocations);

        return "locations/list-locations";
    }
}
