package com.daors.locationcity.dao;

import com.daors.locationcity.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City, Integer> {
}
