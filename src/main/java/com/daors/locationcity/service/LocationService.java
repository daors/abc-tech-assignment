package com.daors.locationcity.service;

import com.daors.locationcity.entity.Location;

import java.util.List;

public interface LocationService {

    List<Location> findAll();

    Location findById(int theId);

    void save(Location theLocation);

    void deleteById(int theId);
}
